-module(prop_item).
-include_lib("proper/include/proper.hrl").
-define(MODEL, model_item).
-compile(export_all).

%%%%%%%%%%%%%%%%%%%%%%%%%
%%% STATEFUL API TEST %%%
%%%%%%%%%%%%%%%%%%%%%%%%%
prop_test() ->
    _ = start_apps(),
    proper:forall(commands(?MODEL), fun(Cmds) ->
        ?TRAPEXIT(begin
            {History, State, Result} = run_commands(?MODEL, Cmds),
            flush_db(),
            ?WHENFAIL(io:format("History: ~p\nState: ~p\nResult: ~p\n",
                                [History,State,Result]),
                      aggregate(command_names(Cmds),
                                Result =:= ok))
        end)
    end).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PARALLEL STATEFUL API TEST %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
prop_parallel_test() ->
    _ = start_apps(),
    proper:forall(parallel_commands(?MODEL), fun(Cmds) ->
        ?TRAPEXIT(begin
            {History, State, Result} = run_parallel_commands(?MODEL, Cmds),
            flush_db(),
            ?WHENFAIL(io:format("History: ~p\nState: ~p\nResult: ~p\n",
                                [History,State,Result]),
                      aggregate(command_names(Cmds),
                                Result =:= ok))
        end)
    end).

seq_len(Cmds) ->
    [length(Cmds)].

seq_range(Cmds) ->
    N = length(Cmds),
    Tens = trunc(N / 10)*10,
    [{Tens,Tens+9}].

unique_cmds(Cmds) ->
    [length(lists:usort( [Call || {set, _, {call, _, Call, _}} <- Cmds]))].


flush_db() ->
    memstore_proc:reset(item_model).

start_apps() ->
    Parent = self(),
    spawn(fun() ->
        application:set_env(lager, log_root, "_build/test/log/", [{persistent, true}]),
        {ok, Apps1} = application:ensure_all_started(crud),
        {ok, Apps2} = application:ensure_all_started(inets),
        Parent ! {ok, Apps1 ++ Apps2},
        timer:sleep(infinity)
    end),
    receive
        {ok, Apps} ->
            Apps
    end.
