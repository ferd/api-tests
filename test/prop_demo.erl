-module(prop_demo).
-include_lib("proper/include/proper.hrl").
-compile(export_all).

%%%%%%%%%%%%%%%%%
%%% EXAMPLE 1 %%%
%%%%%%%%%%%%%%%%%

%% @doc grab the biggest number in a given list
biggest([X]) -> X;
biggest([X | Xs]) -> biggest(Xs, X).

%% @private iterator for `biggest/1'
biggest([], X) -> X;
biggest([Y | Ys], X) ->
    if Y > X -> biggest(Ys, Y);
       Y < X -> biggest(Ys, X)
    end.

%% @doc test for `biggest/1' function
prop_ex1() ->
    proper:forall(nonempty_list(integer()), fun(List) ->
        biggest(List) =:= lists:last(lists:sort(List))
    end).


%%%%%%%%%%%%%%%%%
%%% EXAMPLE 2 %%%
%%%%%%%%%%%%%%%%%
prop_ex2() ->
    proper:forall(csv_data(), fun(CSV) ->
        Encoded = encode(CSV),
        Decoded = decode(Encoded),
        ?WHENFAIL(
           io:format("~nEncoded:~n~s~n~nDecoded:~n~p~n",
                     [Encoded, Decoded]),
           Decoded =:= CSV andalso Encoded =/= CSV
        )
    end).

%% @doc to generate CSV data, we'll just make lists of terms of a fixed
%% length.
csv_data() ->
    non_empty(list( vector(10, union([number(), string()])) )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTUAL IMPLEMENTATION %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% @doc
%% Encode CSV terms. Only supports integers, floats, and strings for now.
%% @end
%% The encoding is done in a stepwise manner starting from rows, and
%% then to specific terms.
-spec encode([[string() | number(), ...]]) -> string().
encode([]) -> "";
encode([List | Lists]) -> encode_row(List) ++ encode(Lists).

%% @doc Decode CSV terms into lists of terms
-spec decode(string()) -> [[string() | number(), ...]].
decode(String) ->
    [decode_row(Row) || Row <- string:tokens(String, "\n")].

%% @private
encode_row([Term]) -> encode_term(Term) ++ "\n";
encode_row([Term|Terms]) -> encode_term(Term) ++ ", " ++ encode_row(Terms).

%% @private
encode_term(Int) when is_integer(Int) -> integer_to_list(Int);
encode_term(Float) when is_float(Float) -> float_to_list(Float);
encode_term(String) -> "\""++String++"\"".


%% @private
decode_row(Row) ->
    [decode_term(Term) || Term <- string:tokens(Row, ",")].

%% @private we decode the term by going int -> float -> string since otherwise
%% we may get confused when an entry is `1 3 5', which is a string but we can't
%% know that ahead of time!
decode_term(" "++Rest) -> decode_term(Rest);
decode_term(Term) ->
    try
        decode_int(Term),
        decode_float(Term),
        decode_string(Term)
    catch
        Return -> Return
    end.

%% @private
decode_int(Term) ->
    try list_to_integer(Term) of
        Val -> throw(Val)
    catch
        _:_ -> no_int
    end.

%% @private
decode_float(Term) ->
    try list_to_float(Term) of
        Val -> throw(Val)
    catch
        _:_ -> no_float
    end.

%% @private
decode_string(String) -> string:strip(String, both, $").

